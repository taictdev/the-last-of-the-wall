using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchRotateControllerWithTransform : MonoBehaviour
{
    [SerializeField]
    private float _x, _y, _rotationSpeed = 10;

    private void OnMouseDrag()
    {
        _x = Input.GetAxis("Mouse X") * _rotationSpeed;
        _y = Input.GetAxis("Mouse Y") * _rotationSpeed;

        transform.Rotate(Vector3.down, _x);
        //transform.Rotate(Vector3.right, _y);
    }
}
