using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AutoConfigMaterial : MonoBehaviour
{
    [SerializeField] private Texture[] textures;
    [SerializeField] private Material[] materials;

    [ContextMenu("Update")]
    public void UpdateMaterial()
    {
        for (int i = 0; i < materials.Length; i++)
        {
            materials[i].mainTexture = textures[i];
            materials[i].SetTexture("_EmissionMap", textures[i]);
        }
        print("DONE");
        Clear();
    }

    [ContextMenu("Clear")]
    public void Clear()
    {
        textures = new Texture[0];
        materials = new Material[0];
    }
}