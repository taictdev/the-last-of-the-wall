using UnityEngine;
using deVoid.UIFramework;
using System;
using UniRx;

namespace DjVoteGame.UI
{
    [Serializable]
    public class LoadingProperties : PanelProperties
    {
        public bool HasBackGround = true;
    }
    public class UILoadingController : APanelController<LoadingProperties>
    {
        [SerializeField] GameObject _bg;
        // Start is called before the first frame update
        void Start()
        {
            Observable.Interval(TimeSpan.FromSeconds(2.0f))
                .TakeUntilDisable(this)
                .Subscribe(_ =>
                {
                    // handle here
                })
                .AddTo(this);
        }

        protected override void OnPropertiesSet()
        {
            base.OnPropertiesSet();
            _bg.SetActive(Properties.HasBackGround);
        }
    }
}

