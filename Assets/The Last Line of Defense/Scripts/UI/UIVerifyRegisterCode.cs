using deVoid.UIFramework;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DjVoteGame.Api.Account;
using DjVoteGame.Common;
using DjVoteGame.Api;

namespace DjVoteGame.UI
{
    public class UIVerifyRegisterCode : APanelController
    {
        [SerializeField] private TMP_InputField _tmpCode;
        [SerializeField] private Button _btnSend;

        protected override void Awake()
        {
            base.Awake();

            _btnSend.onClick.AddListener(OnSend);
        }

        private void OnClose()
        {
            UIFrameManager.Instance.UIFrame.HidePanel(ScreenIds.UIVerifyRegisterCode);
        }

        private async void OnSend()
        {
            try
            {
                UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UIMessageBoxPopup, new UIMessageBoxPopupProperties()
                {
                    Title = StringConst.NOTIFICATION,
                    Message = StringConst.WAITING,
                });

                var res = await new VerifyRegisterAPI()
                {
                    VerifyCode = _tmpCode.text
                }.TryCatchSend();

                UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UIMessageBoxPopup, new UIMessageBoxPopupProperties()
                {
                    Title = StringConst.NOTIFICATION,
                    Message = StringConst.COMPLETE,
                    OkAction = () =>
                    {
                        OnClose();
                    }
                });
            }
            catch (BasicError ex)
            {
                UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UIMessageBoxPopup, new UIMessageBoxPopupProperties()
                {
                    Title = StringConst.NOTIFICATION,
                    Message = ex.message,
                    OkAction = () =>
                    {
                        // do thing
                    }
                });
            }
        }
    }
}
