using UnityEngine;
using DG.Tweening;

public class UIAnimationSlide : MonoBehaviour
{
    private RectTransform _rectTransform;
    private Vector3 _vecOrigin;
    [SerializeField] private Vector3 _vecOffsetOut;
    [SerializeField] private float _duration;
    [SerializeField] private Ease _ease;
    private bool _isMoving = false;

    private void Awake()
    {
        TryGetComponent(out _rectTransform);
        _vecOrigin = _rectTransform.transform.position;
    }

    public void MoveIn()
    {
        if (_isMoving)
            return;
        _isMoving = true;

        _rectTransform.DOLocalMove(_vecOrigin, _duration).SetEase(_ease).SetId(this).OnComplete(() =>
        {
            _isMoving = false;
        });
    }

    public void MoveOut()
    {
        if (_isMoving)
            return;
        _isMoving = true;

        _rectTransform.DOLocalMove(_vecOrigin + _vecOffsetOut, _duration).SetEase(_ease).SetId(this).OnComplete(() =>
        {
            _isMoving = false;
        });
    }

    private void OnDestroy()
    {
        DOTween.Kill(this);
    }
}
