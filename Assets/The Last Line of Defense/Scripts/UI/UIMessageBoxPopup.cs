using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using deVoid.UIFramework;
using System;
using TMPro;
using Cysharp.Threading.Tasks;
using UniRx;
using UnityEditor.ShortcutManagement;

namespace DjVoteGame.UI
{
    [Serializable]
    public class UIMessageBoxPopupProperties : PanelProperties
    {
        public string Title;
        public string Message;
        public string OkMessage;
        public string CancelMessage;
        public Action OkAction;
        public Action CancelAction;

        public UIMessageBoxPopupProperties()
        {
            Title = "NOTIFICATION";
            Message = "Good luck today!";
            OkAction = null;
            CancelAction = null;
            OkMessage = null;
            CancelMessage = null;
        }
    }

    public class UIMessageBoxPopup : APanelController<UIMessageBoxPopupProperties>
    {
        [SerializeField] private TextMeshProUGUI _tmpTitle;
        [SerializeField] private TextMeshProUGUI _tmpMessage;
        [SerializeField] private TextMeshProUGUI _tmpOkMessage;
        [SerializeField] private TextMeshProUGUI _tmpCancelMessage;
        [SerializeField] private Button _btnOk;
        [SerializeField] private Button _btnCancel;

        protected override void OnPropertiesSet()
        {
            base.OnPropertiesSet();

            _tmpTitle.text = Properties.Title;
            _tmpMessage.text = Properties.Message;
            _tmpCancelMessage.text = "CANCEL";
            _tmpOkMessage.text = "OK";

            _btnCancel.onClick.RemoveAllListeners();
            _btnCancel.onClick.AddListener(() =>
            {
                SetActive(false); // You can call function hide of UIFrameManager
                Properties.CancelAction?.Invoke();
            });

            _btnOk.onClick.RemoveAllListeners();
            _btnOk.onClick.AddListener(() =>
            {
                SetActive(false);
                Properties.OkAction?.Invoke();
            });

            if (Properties.CancelMessage != null)
            {
                _tmpCancelMessage.text = Properties.CancelMessage;
            }

            if (Properties.OkMessage != null)
            {
                _tmpOkMessage.text = Properties.OkMessage;
            }

            SetActiveOKButton(Properties.OkAction != null);
            SetActiveCancelButton(Properties.CancelAction != null);
        }

        private void SetActive(bool isActive)
        {
            gameObject.SetActive(isActive);
        }

        private void SetActiveCancelButton(bool isActive)
        {
            _btnCancel.gameObject.SetActive(isActive);
        }

        private void SetActiveOKButton(bool isActive)
        {
            _btnOk.gameObject.SetActive(isActive);
        }
    }
}