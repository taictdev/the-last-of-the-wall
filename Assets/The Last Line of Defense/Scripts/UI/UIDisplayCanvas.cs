using System.Collections;
using System.Collections.Generic;
using deVoid.UIFramework;
using UnityEngine;

public class UIDisplayCanvas : MonoBehaviour
{
    private void LateUpdate()
    {
        if (Camera.main is null) return;
        transform.LookAt(transform.position + Camera.main.transform.forward);
    }
}