using UnityEngine;
using deVoid.UIFramework;
using UnityEngine.UI;
using DjVoteGame.Api.Account;
using DjVoteGame.Common;
using DjVoteGame.Api;
using TMPro;

namespace DjVoteGame.UI
{
    public class UILogin : APanelController
    {
        [SerializeField] private TMP_InputField _tmpEmail;
        [SerializeField] private TMP_InputField _tmpPassword;
        [SerializeField] private Button _btnLogin;
        [SerializeField] private Button _btnQuitGame;
        [SerializeField] private Button _btnRegister;

        protected override void Awake()
        {
            base.Awake();
            _btnLogin.onClick.AddListener(OnLogin);
            _btnQuitGame.onClick.AddListener(OnQuitGame);
            _btnRegister.onClick.AddListener(OnRegister);
        }

        private void OnRegister()
        {
            UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UIRegisterAccount);
        }

        private void OnQuitGame()
        {
            Application.Quit();
        }

        private async void OnLogin()
        {
            try
            {
                UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UIMessageBoxPopup, new UIMessageBoxPopupProperties()
                {
                    Title = StringConst.NOTIFICATION,
                    Message = StringConst.WAITING,
                });

                var loginRes = await new EmailLoginAPI()
                {
                    Email = _tmpEmail.text,
                    Password = _tmpPassword.text
                }.TryCatchSend();

                NetworkManager.Instance.userAuthenData = loginRes.data;

                // Login complete
                UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UIMessageBoxPopup, new UIMessageBoxPopupProperties()
                {
                    Title = StringConst.NOTIFICATION,
                    Message = loginRes.data.token.ToString(),
                    OkAction = () =>
                    {
                        //do nothing
                    }
                });
            }
            catch (BasicError ex)
            {
                // Login error
                UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UIMessageBoxPopup, new UIMessageBoxPopupProperties()
                {
                    Title = StringConst.NOTIFICATION,
                    Message = ex.message,
                    OkAction = () =>
                    {
                        //do nothing
                    }
                });
            }
        }
    }
}