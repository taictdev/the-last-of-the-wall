using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using deVoid.UIFramework;
using TMPro;
using System;
using DjVoteGame.Api.Account;
using DjVoteGame.Common;
using DjVoteGame.Api;

namespace DjVoteGame.UI
{
    public class UIRegisterAccount : APanelController
    {
        [SerializeField] private TMP_InputField _tmpUserName;
        [SerializeField] private TMP_InputField _tmpEmail;
        [SerializeField] private TMP_InputField _tmpPassword;
        [SerializeField] private TMP_InputField _tmpConfirmPassword;
        [SerializeField] private Button _btnRegister;
        [SerializeField] private Button _btnClose;

        protected override void Awake()
        {
            base.Awake();

            _btnRegister.onClick.AddListener(OnRegister);
            _btnClose.onClick.AddListener(OnClose);
        }

        private void OnClose()
        {
            UIFrameManager.Instance.UIFrame.HidePanel(ScreenIds.UIRegisterAccount);
        }

        private async void OnRegister()
        {
            try
            {
                UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UIMessageBoxPopup, new UIMessageBoxPopupProperties()
                {
                    Title = StringConst.NOTIFICATION,
                    Message = StringConst.WAITING,
                });

                var res = await new RegisterAccountAPI()
                {
                    Username = _tmpUserName.text,
                    Email = _tmpEmail.text,
                    Password = _tmpPassword.text,
                    Confirm_password = _tmpConfirmPassword.text,
                }.TryCatchSend();

                UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UIMessageBoxPopup, new UIMessageBoxPopupProperties()
                {
                    Title = StringConst.NOTIFICATION,
                    Message = StringConst.COMPLETE,
                    OkAction = () =>
                    {
                        OnClose();
                    }
                });
            }
            catch (BasicError ex)
            {
                UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UIMessageBoxPopup, new UIMessageBoxPopupProperties()
                {
                    Title = StringConst.NOTIFICATION,
                    Message = ex.message,
                    OkAction = () =>
                    {
                    }
                });
            }
        }
    }
}
