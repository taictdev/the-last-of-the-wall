using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deVoid.UIFramework;
using DjVoteGame.Utils;
using UnityEngine.Rendering.Universal;
using DjVoteGame.Common;

namespace DjVoteGame.UI
{
    public class UIFrameManager : ManualSingletonMono<UIFrameManager>
    {
        [SerializeField] private List<UISettings> _uISettings;

        public UIFrame UIFrame { get; private set; }

        public override void Awake()
        {
            base.Awake();
            foreach (UISettings uiSetting in _uISettings)
            {
                UIFrame = uiSetting.CreateUIInstance(true, UIFrame);
            }

            var cameraData = Camera.main.GetUniversalAdditionalCameraData();
            cameraData.cameraStack.Add(UIFrame.GetComponentInChildren<Camera>());
        }
    }
}