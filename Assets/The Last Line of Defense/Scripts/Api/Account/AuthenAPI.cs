using System;
using Duck.Http.Service;
using Newtonsoft.Json.Linq;
using DjVoteGame.Common;

namespace DjVoteGame.Api.Account
{
    [Serializable]
    public class UserAuthenData
    {
        public string token;
        public string timestamp;
    }

    public class EmailLoginAPI : HttpApi<BasicData<UserAuthenData>>
    {
        public string Email;
        public string Password;
        protected override string ApiUrl => "/api/v1/user/authentication/emails/login";

        protected override IHttpRequest GetHttpRequest()
        {
            JObject param = new JObject();
            param.Add("email", Email);
            param.Add("password", Password);
            return NetworkManager.Instance.HttpPostNoAuthen(NetworkManager.Instance.apiServer, ApiUrl, param);
        }
    }
}