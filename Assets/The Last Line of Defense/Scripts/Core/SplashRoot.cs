using System.Collections;
using System.Collections.Generic;
using DjVoteGame.Core;
using UnityEngine;
using DjVoteGame.UI;
using DjVoteGame.Common;
namespace DjVoteGame.Core
{
    public class SplashRoot : BaseRoot
    {
        protected override void Start()
        {
            base.Start();

            UIFrameManager.Instance.UIFrame.ShowPanel(ScreenIds.UILogin);
            NetworkManager.Instance.InitEnvironment();
        }
    }
}

