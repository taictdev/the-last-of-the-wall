namespace DjVoteGame.Common
{
    public enum SceneName
    {
        None,
        Splash,
    }

    public enum ItemType
    {
        Item_1 = 1,
    }

    public static class StringConst
    {
        public static string PLAYEEPREF_EMAIL_KEY = "PLAYEEPREF_EMAIL_KEY";
        public static string PLAYERPREF_PASSWORD_KEY = "PLAYERPREF_PASSWORD_KEY";
        public static string NOTIFICATION = "Notification";

        public static string WAITING = "Loading...";
        public static string COMPLETE = "Thanks";
    }
}