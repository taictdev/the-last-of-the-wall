using UnityEngine;
using DjVoteGame.Common;

namespace DjVoteGame.Config
{
    [CreateAssetMenu(fileName = "InventoryItemConfig", menuName = "ScriptableObjects/InventoryItemConfig", order = 1)]
    public class InventoryItemConfig : ScriptableObject
    {
        public ItemType ItemType;
        public Sprite Icon;
        public string Name;
        public string Description
        { get { return GetDescription(); } }

        public bool IsCanStack;
        public bool IsConsumable;
        [SerializeField] private string[] m_descriptions;

        private string GetDescription()
        {
            string temp = "";
            foreach (string str in m_descriptions)
            {
                temp += str + "\n";
            }
            return temp;
        }
    }
}